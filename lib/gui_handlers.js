function Game() {
  this.player = Game.marks[0];
  this.board = this.makeBoard();
}

Game.marks = ["x", "o"];

Game.prototype.makeBoard = function () {
  return _.times(3, function (i) {
    return _.times(3, function (j) {
      return null;
    });
  });
};

Game.prototype.move = function (pos) {
  if ( this.board[pos[0]][pos[1]] != null ) {
    return false;
  }

  this.board[pos[0]][pos[1]] = this.player;
  this.player = (this.player == Game.marks[0]) ? (Game.marks[1]) : (Game.marks[0])
  return true;
};

Game.prototype.winner = function () {
  return ( this.diagonalWinner() || this.horizontalWinner() || this.verticalWinner() );
};

Game.prototype.diagonalWinner = function () {
  var game = this;

  var diagonalPositions1 = [[0, 0], [1, 1], [2, 2]];
  var diagonalPositions2 = [[2, 0], [1, 1], [0, 2]];

  var winner = null;
  _(Game.marks).each(function (mark) {
    function didWinDiagonal(diagonalPositions) {
      return _.every(diagonalPositions, function (pos) {
        return game.board[pos[0]][pos[1]] === mark;
      });
    }

    var won = _.any(
      [diagonalPositions1, diagonalPositions2],
      didWinDiagonal
    );

    if (won) {
      winner = mark;
    }
  });

  return winner;
};

Game.prototype.horizontalWinner = function () {
  var game = this;

  var winner = null;
  _(Game.marks).each(function (mark) {
    var indices = _.range(0, 3);

    var won = _(indices).any(function (i) {
      return _(indices).every(function (j) {
        return game.board[i][j] === mark;
      });
    });

    if (won) {
      winner = mark;
    }
  });

  return winner;
};

Game.prototype.verticalWinner = function () {
  var game = this;

  var winner = null;
  _(Game.marks).each(function (mark) {
    var indices = _.range(0, 3);

    var won = _(indices).any(function (j) {
      return _(indices).every(function (i) {
        return game.board[i][j] === mark;
      });
    });

    if (won) {
      winner = mark;
    }
  });

  return winner;
};

var guiGame = new Game();

$(function() {
	
	var clearBoard = function() {
		$('.square').removeClass('clickedX')
		            .removeClass('clickedO')
								.text('')
								.addClass('unclicked');
	}
	
  $('.unclicked').on("click", function() {
    var cellNumber = parseInt($(this).data('cell'));

    var row = Math.floor(cellNumber / 3);
    var column = cellNumber % 3;
    var position = [row, column];

    var currentPlayer = guiGame.player;
    guiGame.move(position);

    if (currentPlayer == "x") {
      $(this).removeClass("unclicked").addClass("clickedX");
      $(this).text("X");
    } else if (currentPlayer == "o") {
      $(this).removeClass("unclicked").addClass("clickedO");
      $(this).text("O");
    }

    var winner = guiGame.winner();
    if (winner) {
			var score = parseInt( $('#' + winner.toUpperCase() + 'score').text() );
			$('#' + winner.toUpperCase() + 'score').text(score + 1);
			clearBoard();
			guiGame = new Game();
    }
  });

	$('.new_game').on('click', function() {
		clearBoard();
		guiGame = new Game();
	});
});